const { Pool, Client } = require('pg');

const client = new Client({
  user: 'postgres',
  host: 'localhost',
  database: 'new',
  password: '',
  port: 5432,
});

client.connect();

client.on('error', (err, client) => {
  console.error('Unexpected error on idle client', err);
  process.exit(-1)
});

module.exports = client;
