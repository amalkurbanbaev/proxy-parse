const cheerio = require('cheerio');
const request = require('request');
// const moment = require('moment');

async function getPage(url) {
  return new Promise((resolve, reject) => {
    request({
      url: url,
      headers: {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36'
      }
    }, (error, response, body) => {
      if (error) {
        return reject(error)
      }
      return resolve(cheerio.load(body, {decodeEntities: false}));
    });
  });
}


const { Pool, Client } = require('pg');

const client = new Client({
    user: 'postgres',
    host: 'localhost',
    database: 'new',
    password: '',
    port: 5432,
});

client.connect();

client.on('error', (err, client) => {
    console.error('Unexpected error on idle client', err);
    process.exit(-1)
});


async function getProxiesFromPage(url, page) {
  let result = [];
  let ips = [];
  let ports = [];

  const $ = await getPage(url);

  const proxies = $('.spy1x').map(function(i, el) {
    let ip = $(this)
      .find('td:nth-child(1)')
      .text();
    ips.push(ip);
  });
ips.splice(0, 1);
}

async function run(url) {
      // нужно записывать в таблицу через цикл
      const result = [];
      const ips = await getProxiesFromPage(url, 1);
      let total = ips.length;
      for (let i=0; i<total; i++) {
        const text = 'INSERT INTO proxy(ip) VALUES($1) RETURNING *';
             const values = ips;
             await client
               .query(text, values)
               .then(res => {
                 console.log(res.rows)
               })
               .catch(e => console.error(e.stack));
      }
      return result;
}

module.exports = async function (url) {
  try {
    let result = [];
    const proxies = await getProxiesFromPage(url, 1);
    run(url);
  } catch (e) {
      throw e;
  }
};