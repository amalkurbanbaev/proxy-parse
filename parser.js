const cheerio = require('cheerio');
const request = require('request');
const moment = require('moment');

async function getPage(url) {
  return new Promise((resolve, reject) => {
    request({
      url: url,
      headers: {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36'
      }
    }, (error, response, body) => {
      if (error) {
        return reject(error)
      }
      return resolve(cheerio.load(body, {decodeEntities: false}));
    });
  });
}

let ips = [];

async function getProxiesFromPage(url, page) {
  let result = [];
  let ports = [];
  const $ = await getPage(url);

  const proxies = $('.spy1x').map(function(i, el) {
    let ip = $(this)
      .find('td:nth-child(1)')
      .text();
    ips.push(ip);
  });
  ips.splice(0, 1);

  console.log(`Page ${page}: found ${ips.length}`); // неправильный селектор
  const nextPage = $('a:nth-child(15)');
  // Go to next page by url from "next page" button href
  if (nextPage.get(0)) {
    const nextProxies = await getProxiesFromPage(nextPage.attr('href'), ++page);
    result = result.concat(nextProxies);
  }
}


module.exports = async function (url) {
  try {
    let result = [];
    const proxies = await getProxiesFromPage(url, 1)
  } catch (e) {
      throw e;
  }
};